/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edutech;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Alberto
 */
public class Grupo {

    private int idGrupo;
    private String nombre;
    private List<Alumno> alumnos = new ArrayList();
    private int puntos;

    public Grupo(int id, String nombre) {
        this.idGrupo = id;
        this.nombre = nombre;
        puntos = 0;
    }

    public int getIdGrupo() {
        return idGrupo;
    }

    public String getNombre() {
        return nombre;
    }

    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }

    public void sumaPuntos(int puntos) { // Suma los puntos por respuesta correcta;
        this.puntos += puntos;
    }

    public void restaPuntos(int puntos) { //Resta los puntos por respuesta incorrecta;
        this.puntos -= puntos;
    }

}
