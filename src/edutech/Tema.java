package edutech;

import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Alberto
 */
public class Tema{
 private int idTema;
    private String tema;
    private String descripciontema;
    private List<Cuestionario> cuestionarios = new ArrayList ();

    public Tema() {
    }
    
    public Tema(int idTema, String tema, String descripciontema) {
        this.idTema = idTema;
        this.tema = tema;
        this.descripciontema = descripciontema;
    }

    public int getIdTema() {
        return idTema;
    }

    public void setIdCuestionario(int idTema) {
        this.idTema = idTema;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    public String getDescripciontema() {
        return descripciontema;
    }

    public void setDescripciontema(String descripciontema) {
        this.descripciontema = descripciontema;
    }

    public List<Cuestionario> getCuestionarios() {
        return cuestionarios;
    }

    public Cuestionario getCuestionario(int puntero) {
        return cuestionarios.get(puntero);
    }
    
    /*public void setCuestionario(List<Cuestionario> cuestionario) {
        this.cuestionario = cuestionario;
    }*/
   
    public void agregaCuestionario(Cuestionario cuestionario) { //Agrega una pregunta al cuestionario.
        cuestionarios.add(cuestionario);
    }
    
    public void eliminaCuestionario(Cuestionario cuestionario) { //Elimina una pregunta del cuestionario.
        cuestionarios.remove(cuestionario);
    }
    
    public void reemplazaCuestionario(int indice, Cuestionario cuestionario) {
        cuestionarios.add(indice, cuestionario);
    }
}
   

